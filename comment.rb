
require 'rubygems'
require 'nokogiri'  
require 'pry'
require_relative 'post'

class Comment < Post

def initialize
  super
end


def get_comments
@@doc.search('.comment').map do |element|
element.inner_text
end
# search('.subtext > a:nth-child(2)').map { |span| span.inner_text}[0]
end

def extract_usernames
  @@doc.search('.comhead > a:first-child').map do |element|
    element.inner_text
  end
end

def extract_days_ago_comment
  @@doc.search('.comhead > a:second-child').map do |element|
    element.inner_text
  end
end

end
